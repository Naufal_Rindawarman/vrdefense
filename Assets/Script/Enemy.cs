﻿using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour, IDestroyable {

	public int maxHealth = 100;
	protected int health = 100;
	public int maxAttackRange = 10;
	public int damage = 2;
	public float speed = 3f;
	public bool isAlive = true;

	public float fireRate = 3f;

	public int scoreValue = 100;

	public Projectile projectile;

	public Transform target;

	protected bool engageTarget = false;
	protected int forwardMove = 0;
	protected int rightMove = 0;

	protected string state = "Closing";

	protected float waitForShoot = 0f;

	protected Vector3 targetPosition;

	protected float startAltitude;

	[HideInInspector]
	public GameObject indicator;

	protected void Start(){
		health = maxHealth;
		projectile.damage = damage;
		Vector3 initPos = transform.position;
		startAltitude = initPos.y;
		targetPosition = target.position;
		targetPosition.y = startAltitude;
	}

	public void TakeDamage(int dam){
		if (!isAlive) return;

		health -= dam;

		if (health <= 0){
			Dead();
			isAlive = false;
		}
	}

	public void ForceKill(){
		TakeDamage (maxHealth);
	}

	protected void Dead(){
		MyLevelManager.instance.AddScore (scoreValue);
		isAlive = false;
		CancelInvoke("Think");
		ParticleHitPoolerManager.instance.PlayParticleHit ("Explosion", transform.position);
		state = "Dead";
		Destroy (indicator);
		gameObject.layer = 12;
		//Rigidbody rb = GetComponent<Rigidbody>();
		//rb.useGravity = true;
		StartCoroutine(Crash());

	}

	void LookAt(){
		transform.LookAt(target);
	}

	protected void ShootRay(){
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit)){
			Enemy enemy = hit.transform.GetComponent<Enemy>();
			if (enemy != null){
				enemy.TakeDamage(20);
			}
		}
	}

	protected void Shoot(){
		projectile.Shoot();
	}

	public virtual void Think(){

		float distance = Vector3.Distance(transform.position, target.position);
		if (distance <= maxAttackRange){
			engageTarget = true;
			state = "Engaging";
			Debug.Log ("Engage!");
		} else {
			engageTarget = false;
			targetPosition = target.position;
			targetPosition.y = startAltitude;
			state = "Closing";
		}
	}

	protected void NewTargetPosition(){
		NewTargetPosition (359, maxAttackRange);
	}

	protected void NewTargetPosition(int angle, int distance){
		int angle2 = Random.Range (0, angle);
		float distance2 = Random.Range (0.5f, 1.5f)*distance;
		Vector3 newTargetPos = target.position + new Vector3 (distance2*Mathf.Sin(angle2), 0, distance2*Mathf.Cos(angle2));
		newTargetPos.y = startAltitude;
		targetPosition = newTargetPos;
	}

	protected virtual IEnumerator Crash(){
		Vector3 pivot = transform.right;
		float dur = 10f;
		float scale = Random.Range(0.5f, 1f);
		int rand = Random.Range(0,10);
		if (rand < 6){
			scale *= -1;
			pivot *= -1;
		}
		float fallSpeed = 0f;
		while (dur > 0){
			transform.Rotate(Vector3.up*scale*30*Time.deltaTime);
			transform.Rotate(Vector3.right*scale*7*Time.deltaTime, Space.World);
			transform.RotateAround(pivot, Vector3.up, 2f*scale*Time.deltaTime);
			transform.Translate (Vector3.up * -fallSpeed*Time.deltaTime, Space.World);
			dur -= Time.deltaTime;
			fallSpeed += 9.8f*Time.deltaTime;
			yield return null;
		}
		DestroyEnemy ();
	}

	protected virtual void ToDo(){
		//transform.Translate(rightMove*Time.deltaTime, 0, forwardMove*Time.deltaTime);
		if (state == "Closing"){
			//transform.LookAt(target.transform);
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetPosition-transform.position), 10*Time.deltaTime);
			transform.Translate(Vector3.forward*speed*Time.deltaTime);
		}

		if (state == "Engaging"){
			//transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.position-transform.position), Time.deltaTime);
			projectile.transform.LookAt(target.transform);
			if (waitForShoot <= 0f){
				Shoot();
				waitForShoot = fireRate;
			}
		}

		if (state == "Dead"){
			
		}

	}

	void OnTriggerEnter(Collider other){
		Debug.Log ("Collision");
		if (other.gameObject.layer == 31) {
			DestroyEnemy ();
		}
	}

	void DestroyEnemy(){
		ParticleHitPoolerManager.instance.PlayParticleHit ("Explosion", transform.position);
		MyLevelManager.instance.RemoveEnemy(this);
		Destroy (gameObject);
	}

	public void RemoveFromMap(){
		Destroy (indicator);
		Destroy (gameObject);
	}
		
}
