﻿using UnityEngine;

public class GameManager : MonoBehaviour {

	private static GameManager _instance;
	public static GameManager instance {
		get {
			if (_instance == null) {
				_instance = new GameObject ("GameManager").AddComponent<GameManager> ();
			} 
			return _instance;
		}
	}
		

	void Awake(){
		if (_instance == null) {
			_instance = this;
		} else if (_instance != this) {
			Destroy (this);
		}
	}

	void Start(){
		DontDestroyOnLoad (gameObject);	
	}
		
}


