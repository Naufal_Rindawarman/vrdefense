﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyLevelManager : MonoBehaviour {

	public Player player;
	public int score = 0;
	public Spawner spawner;
	public GameOverHandler gameOver;

	private List<Enemy> enemies;

	public int maxEnemyAlive = 2;

	public int enemyAlive = 0;

	public bool isGameRunning = true;

	private static MyLevelManager _instance;
	public static MyLevelManager instance {
		get {
			if (_instance == null) {
				_instance = new GameObject ("LevelManager").AddComponent<MyLevelManager> ();
			} 
			return _instance;
		}
	}

	void Awake(){
		if (_instance == null) {
			_instance = this;
			score = 0;
			enemies = new List<Enemy> ();

		} else if (_instance != this) {
			Destroy (this);
		}
	}

	public void AddEnemy(Enemy enemy){
		enemies.Add (enemy);
		enemyAlive++;
		Debug.Log ("Enemies alive: " + enemyAlive);
	}

	public void RemoveEnemy(Enemy enemy){
		enemies.Remove(enemy);
		enemyAlive--;
		Debug.Log ("Enemies alive: " + enemyAlive);
	}

	public int GetEnemiesCount(){
		return enemies.Count;
	}

	public void AddScore(int n){
		score += n;
		player.UpdateScoreText ();
	}

	public void GameOver(){
		gameOver.StartGameOver ();
	}

	public void RestartBegin(){
		//SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		while (enemies.Count > 0) {
			Enemy en = enemies [0];
			enemies.RemoveAt (0);
			en.RemoveFromMap ();
		}
		spawner.enabled = false;
		isGameRunning = false;
		enemyAlive = 0;
	}

	public void RestartFinish(){
		score = 0;
		spawner.enabled = true;
		player.Reset ();
		isGameRunning = true;
	}

}


