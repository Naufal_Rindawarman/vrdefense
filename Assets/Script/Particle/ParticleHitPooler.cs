﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ParticleHitPooler {

	public string tagName;
	public AudioClip clip;
	public ParticleSystem particlePrefab;
	public int objectCount = 10;
	public int particleEmitCount = 1;

	private ParticleHit[] particleHits;
	private int currentIndex = 0;

	// Use this for initialization
	public void Initialize (GameObject container) {
		particleHits = new ParticleHit[objectCount];
		for (int i = 0; i < objectCount; i++) {
			GameObject obj = GameObject.Instantiate (particlePrefab.gameObject, container.transform.position, container.transform.rotation, container.transform);

			if (obj.GetComponent<AudioSource> () == null) {
				obj.AddComponent<AudioSource> ().clip = clip;
			}
				
			ParticleHit phit = obj.AddComponent<ParticleHit> ();
			phit.SetAudioClip (clip);
			particleHits [i] = phit;
			//phit.SetAudioClip (clip);
		}

		//PlayHitParticle ();
	}

	public void PlayHitParticle(){
		if (currentIndex >= objectCount) {
			currentIndex = 0;
		}

		particleHits [currentIndex].PlayHitParticle ();
	}

	public void PlayHitParticle(Vector3 position){
		PlayHitParticle(position, Quaternion.Euler(Vector3.up), particleEmitCount);
	}

	public void PlayHitParticle(Vector3 position, Quaternion rotation){
		PlayHitParticle(position, rotation, particleEmitCount);
	}

	public void PlayHitParticle(Vector3 position, Quaternion rotation, int quantity){
		if (currentIndex >= objectCount) {
			currentIndex = 0;
		}
		particleHits [currentIndex].transform.position = position;
		particleHits [currentIndex].transform.rotation = rotation;
		particleHits [currentIndex].PlayHitParticle (quantity);
	}
}
