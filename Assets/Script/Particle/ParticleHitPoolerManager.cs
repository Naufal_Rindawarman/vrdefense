﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ParticleHitPoolerManager : MonoBehaviour {

	private static ParticleHitPoolerManager _instance;
	public static ParticleHitPoolerManager instance {
		get {
			if (_instance == null) {
				_instance = new GameObject ("Particle Pooler Manager").AddComponent<ParticleHitPoolerManager> ();
			} 
			return _instance;
		}
	}

	public int count = 10;


	public ParticleHitPooler[] particlePoolers;


	private Dictionary<string, ParticleHitPooler> particleDictionary;

	void Awake(){
		if (_instance == null) {
			_instance = this;
		} else if (_instance != this) {
			Destroy (this);
		}
	}

	void Start(){
		particleDictionary = new Dictionary<string, ParticleHitPooler> ();
		for (int i = 0; i<particlePoolers.Length; i++) {
			particlePoolers [i].Initialize (new GameObject (particlePoolers [i].tagName + " Hit Particle Pool"));
			particleDictionary.Add (particlePoolers [i].tagName, particlePoolers [i]);
		}

		//PlayParticleHit ("Terrain", Vector3.zero);
	}

	public void PlayParticleHit(string tag, Vector3 position){
		PlayParticleHit(tag, position, Quaternion.Euler(Vector3.up));
	}

	public void PlayParticleHit(string tag, Vector3 position, Quaternion rotation){
		ParticleHitPooler poolers;
		if (particleDictionary.TryGetValue (tag, out poolers)) {
			poolers.PlayHitParticle (position, rotation);
		}
	}
}


