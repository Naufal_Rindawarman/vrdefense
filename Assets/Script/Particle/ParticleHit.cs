﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(ParticleSystem))]
public class ParticleHit : MonoBehaviour {

	// Use this for initialization
	private AudioSource source;
	private ParticleSystem particleSys;

	void Awake () {
		source = GetComponent<AudioSource> ();
		source.Stop ();
		particleSys = GetComponent<ParticleSystem> ();
		particleSys.Stop ();

		source.spatialBlend = 1f;
	}

	public void PlayHitParticle(){
		PlayHitParticle (1);
	}

	public void PlayHitParticle(int i){
		source.Play ();
		particleSys.Emit (i);
	}

	public void SetAudioClip(AudioClip clip){
		source.clip = clip;
	}

}
