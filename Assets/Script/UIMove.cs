﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMove : MonoBehaviour {

	public Transform target;

	public bool smooth = false;

	public float translateSpeed = 5f;
	public float rotateSpeed = 5f;

	void Start(){
		if (!target) {
			this.enabled = false;
		}
	}

	// Update is called once per frame
	void LateUpdate () {
		if (smooth) {
			transform.position = Vector3.Lerp (transform.position, target.position, translateSpeed * Time.deltaTime);
			transform.rotation = Quaternion.Lerp (transform.rotation, target.rotation, rotateSpeed * Time.deltaTime);
			return;
		}

		transform.position = target.position;
		transform.rotation = target.rotation;


	}
}
