﻿using System.Collections;
using UnityEngine;

public class Aircraft : Enemy {


	void Start(){
		//ps = GetComponent<ParticleSystem>();
		base.Start();

		NewTargetPosition ();
		MyLevelManager.instance.AddEnemy(this);
		InvokeRepeating ("CheckForwardCollision", 1.5f, 2f);
		//InvokeRepeating ("CheckCeilingCollision", 1.5f, 1f);
		InvokeRepeating ("CheckFloorCollision", 1.7f, 2f);
	}

	void Update () {
		if (isAlive) {
			transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.LookRotation (targetPosition - transform.position), 1f * Time.deltaTime);
			ToDo ();
		}
		waitForShoot -= Time.deltaTime;
		transform.Translate (Vector3.forward * speed * Time.deltaTime);
	}

	void CheckForwardCollision(){
		if (!isAlive) {
			CancelInvoke ("CheckForwardCollision");
			return;
		}
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit)){
			if (hit.transform.gameObject.layer == 31 &&  hit.distance<= 10){
				NewTargetPosition ();
			}
		}
	}

	void CheckCeilingCollision(){
		RaycastHit hit;
		if (Physics.Raycast (transform.position, transform.up, out hit)) {
			if (hit.transform.gameObject.layer == 31 &&  hit.distance<=0.5f) {
				startAltitude -= 0.05f;
				targetPosition.y = startAltitude;
			}
		}
	}

	void CheckFloorCollision(){
		RaycastHit hit;
		if (Physics.Raycast (transform.position, -transform.up, out hit)) {
			if (hit.transform.gameObject.layer == 31) {
				if (hit.distance <= 5f) {
					startAltitude += 0.05f;
					targetPosition.y = startAltitude;
				} else if (hit.distance >= 20f) {
					startAltitude -= 0.05f;
					targetPosition.y = startAltitude;
				}
			}
		}
	}

	protected override void ToDo(){
		if (Vector3.Distance (targetPosition, transform.position) < 50) {
			NewTargetPosition ();
		}

		projectile.transform.LookAt(target.transform);
		if (Vector3.Distance(transform.position, target.position)<=maxAttackRange){
			if (waitForShoot <= 0f){
				Shoot();
				waitForShoot = fireRate;
			}
		}

	}



	protected override IEnumerator Crash(){
		Debug.Log ("aircraft destroyed");
		float dur = 10f;
		float scale = Random.Range(0.5f, 1f);
		int rand = Random.Range(0,10);
		if (rand < 6){
			scale *= -1;
		}
		float fallSpeed = 0f;
		while (dur > 0){
			transform.Rotate(Vector3.forward*scale*30*Time.deltaTime);
			transform.Rotate(Vector3.right*30*Time.deltaTime);
			transform.Translate (Vector3.up * -fallSpeed*Time.deltaTime, Space.World);
			dur -= Time.deltaTime;
			fallSpeed += 4.9f*Time.deltaTime;
			yield return null;
		}
		MyLevelManager.instance.RemoveEnemy(this);
		Destroy(gameObject);
	}
}
