﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Projectile : MonoBehaviour {

	public int damage = 10;
	public AudioClip clip;

	private AudioSource audioSource;
	private List<ParticleCollisionEvent> collisionEvents;
	private ParticleSystem part;

	void Awake(){
		part = GetComponent<ParticleSystem> ();

	}

	void Start(){
		collisionEvents = new List<ParticleCollisionEvent>();
		audioSource = GetComponent<AudioSource> ();
		audioSource.playOnAwake = false;
		if (clip) {
			audioSource.clip = clip;
		}
		
	}

	public void Shoot(){
		part.Emit (1);
		audioSource.Play ();
	}

	// Use this for initialization
	void OnParticleCollision(GameObject go){
		int numCollisionEvents = part.GetCollisionEvents(go, collisionEvents);
		var emitParams = new ParticleSystem.EmitParams();
		int i = 0;
		while (i < numCollisionEvents){
			IDestroyable dest = go.GetComponent<IDestroyable>();
			if (dest != null){
				dest.TakeDamage(damage);
				//Debug.Log("Player hit");
			}
			ParticleHitPoolerManager.instance.PlayParticleHit (go.tag, collisionEvents [i].intersection, Quaternion.LookRotation(collisionEvents[i].normal-collisionEvents [i].intersection));
			i++;
		}
			
	}

}
