﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverHandler : MonoBehaviour {

	// Use this for initialization
	public Text scoreText;
	public Text countDownText;

	public int countDown = 3;

	private int currentCountDown = 3;

	void Awake () {
		//gameObject.SetActive (false);
	}

	public void UpdateScore(){
		scoreText.text = MyLevelManager.instance.score.ToString();
	}

	public void StartGameOver(){
		gameObject.SetActive (true);
		currentCountDown = countDown;
		countDownText.text = "Restart in " + currentCountDown + " second";
		UpdateScore ();
		MyLevelManager.instance.RestartBegin ();
		InvokeRepeating ("RestartCountDown", 0f, 1f);
	}

	void Reset(){
		currentCountDown = countDown;
		CancelInvoke ("RestartCountDown");
		gameObject.SetActive (false);
	}
	
	void RestartCountDown(){
		if (currentCountDown < 0) {
			MyLevelManager.instance.RestartFinish ();
			Reset ();
			return;
		}
		countDownText.text = "Restart in " + currentCountDown + " second";
		currentCountDown--;
	}
}
