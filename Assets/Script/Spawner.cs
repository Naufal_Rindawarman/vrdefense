﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public Enemy[] enemyPrefab;

	public int range = 500;

	public Transform target;

	private float ceiling = 2f;
	private float floor = 2f;
	private float middlePoint = 1f;


	// Use this for initialization
	void Start () {
		InvokeRepeating("Spawn", 1.5f, 5f);
		//InvokeRepeating("InitializeCeiling", 0.5f, 3f);
		//InvokeRepeating("InitializeFloor", 1f, 3f);
	}
	
	void Spawn(){

		if (MyLevelManager.instance.enemyAlive >= MyLevelManager.instance.maxEnemyAlive) {
			return;
		}

		if (!MyLevelManager.instance.isGameRunning) {
			return;
		}

		int idx = Random.Range (0, enemyPrefab.Length);

		int angleHorizontal = Random.Range(0, 359);

		int altitude = Random.Range(15, 30);
		if (enemyPrefab [idx].GetType () == typeof(Aircraft)) {
			altitude = Random.Range(40, 65);
			Debug.Log ("Aircraft, spawn higher");
		}

		int angle = Random.Range(0, 359);
		Enemy enemy = (Enemy) Instantiate(enemyPrefab[idx], target.position + new Vector3(range*Mathf.Cos(angle), target.position.y + altitude, range*Mathf.Sin(angle)), transform.rotation);
		enemy.transform.SetParent (transform);
		enemy.target = target;
	}

	void SpawnRaycast(){

		if (MyLevelManager.instance.enemyAlive >= MyLevelManager.instance.maxEnemyAlive) {
			return;
		}

		if (!MyLevelManager.instance.isGameRunning) {
			return;
		}

		int idx = Random.Range (0, enemyPrefab.Length);

		RaycastHit hit;
		int angleHorizontal = Random.Range(0, 359);

		int angleVertical = Random.Range(5, 15);
		if (enemyPrefab [idx].GetType () == typeof(Aircraft)) {
			angleVertical = Random.Range(40, 50);
			Debug.Log ("Aircraft, spawn higher");
		}
		Vector3 direction = Camera.main.transform.position + new Vector3 (Mathf.Sin(angleHorizontal), Mathf.Sin(angleVertical) , Mathf.Cos(angleHorizontal));
		if (Physics.Raycast (Camera.main.transform.position, direction, out hit)) {
			Debug.Log ("Hit " + hit.transform.gameObject.layer);
			if (hit.transform.gameObject.layer == 31) {
				Enemy enemy = (Enemy)Instantiate (enemyPrefab[idx], hit.point+hit.normal, Quaternion.LookRotation (hit.normal));
				enemy.transform.SetParent (transform);
				enemy.target = target;
			}
		} else {
			//Spawn ();
		}

	}

	void InitializeCeiling(){
		RaycastHit hit;
		if (Physics.Raycast (Camera.main.transform.position, Camera.main.transform.up, out hit)) {
			if (hit.transform.gameObject.layer == 31) {
				ceiling = hit.point.y;
				middlePoint = (ceiling - floor) / 4;
				CancelInvoke ("InitializeCeiling");
			}
		}
	}

	void InitializeFloor(){
		RaycastHit hit;
		if (Physics.Raycast (Camera.main.transform.position, -Camera.main.transform.up, out hit)) {
			if (hit.transform.gameObject.layer == 31) {
				floor = hit.point.y;
				middlePoint = (ceiling - floor) / 4;
				CancelInvoke ("InitializeFloor");
			}
		}
	}

	private void InstantiateEnemy(){

	}

}
