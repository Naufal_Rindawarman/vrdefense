﻿
using UnityEngine;

public class Shooter : MonoBehaviour {

	public Transform moveObject;
	public Transform camera;

	public float moveSpeed = 3f;

	public float fireRate = 0.2f;

	public Projectile projectile;

	private float waitToShoot = 0f;

	// Use this for initialization
	void Start () {

        // Allow Click Everywhere
       
    }
	
	// Update is called once per frame
	void Update () {
		if (GvrControllerInput.ClickButton || Input.GetButton("Fire1")) {
			if (waitToShoot <= 0f) {
				ShootParticle ();
				waitToShoot = fireRate;
			}
		}

		moveObject.Translate (Input.GetAxis("Vertical")*camera.forward*moveSpeed*Time.deltaTime);
		moveObject.Translate (Input.GetAxis("Horizontal")*camera.right*moveSpeed*Time.deltaTime);

		waitToShoot -= Time.deltaTime;
	}



	void ShootRayCast(){
		RaycastHit hit;
		Debug.Log("NEMBAK");
		if (Physics.Raycast(transform.position, transform.forward, out hit)){
			Enemy enemy = hit.transform.GetComponent<Enemy>();
			Debug.Log(hit.transform.name);
			if (enemy != null){
				enemy.TakeDamage(20);
			}
		}
	}

	void ShootParticle(){
		projectile.Shoot ();
	}
}
