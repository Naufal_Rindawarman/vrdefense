﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Shooter))]
public class Player : MonoBehaviour, IDestroyable {

	public int maxHealth = 100;


	public Slider slider;
	public Image[] damageScreen;
	public Text scoreText;

	private float healthFrac = 1f;
	private int health = 100;
	private bool isAlive = true;

	private Shooter shooter;

	private int damageScreenIdx = 0;

	void Start(){
		health = maxHealth;
		shooter = GetComponent<Shooter> ();
	}

	public void TakeDamage(int dam){
		if (!isAlive) {
			return;
		}
			
		health -= dam;
		healthFrac = Mathf.Clamp01((float) (health)/maxHealth);

		slider.value = healthFrac;

		StopCoroutine (DamageScreen (damageScreenIdx));
		StartCoroutine (DamageScreen (damageScreenIdx));
		damageScreenIdx++;
		if (damageScreenIdx >= damageScreen.Length) {
			damageScreenIdx = 0;
		}

		if (health <= 0){
			Dead();
		}
	}

	IEnumerator DamageScreen(int idx){
		Color color = damageScreen[idx].color;
		color.a = 1f;
		while (color.a > 0f) {
			color.a -= 0.5f * Time.deltaTime;
			damageScreen[idx].color = color;
			yield return null;
		}
	}

	public void UpdateScoreText (){
		scoreText.text = "Score: " + MyLevelManager.instance.score;
	}

	void Dead(){
		isAlive = false;
		shooter.enabled = false;
		MyLevelManager.instance.GameOver ();
	}

	public void Reset(){
		health = maxHealth;
		isAlive = true;
		healthFrac = Mathf.Clamp01((float) (health)/maxHealth);
		slider.value = healthFrac;
		shooter.enabled = true;
		UpdateScoreText ();
	}
}
