﻿
public interface IDestroyable {

	void TakeDamage(int dam);
}
